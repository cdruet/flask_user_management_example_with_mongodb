## Overview

This project is a fork from [Patrick Kennedy's project](https://gitlab.com/patkennedy79/flask_user_management_example).

This Flask application contains the basic user management functionality (register, login, logout) to demonstrate how to test a Flask project using [pytest](https://docs.pytest.org/en/stable/) when using mongodb;

For details on how to test a Flask app using pytest, check out Patrick's blog post on [TestDriven.io](https://testdriven.io/):

* [https://testdriven.io/blog/flask-pytest/](https://testdriven.io/blog/flask-pytest/)

## Motivation

I've been developing a Flask application using mongodb in the background. Struggling with pytesting it, I found Patrick's blog referring to this example. To check that things were runnning smoothly as they should with mongodb, I've adjusted the example to work with mongodb. I learned a few things in the process about pytest and figured out how to use it to test Flask applications.

I've offered to Patrick to integrate the mongodb version of the example in its repository. For various reasons that we should completely respect, he prefers not to. Therefore, I choose to publish this revisisted example on my own as it can be useful for anyone facing the same challenge as I did, i.e. testing a Flask app using mongodb.

## Installation Instructions

### Installation

Pull down the source code from this GitLab repository:

```sh
$ git clone git@gitlab.com:cdruet/flask_user_management_example_with_mongodb.git
```

Create a new virtual environment:

```sh
$ cd flask_user_management_example
$ python3 -m venv .venv
```

Activate the virtual environment:

```sh
$ source .venv/bin/activate
```

Install the python packages specified in requirements.txt:

```sh
(.venv) $ pip install -r requirements.txt
```

### Database Initialization

This Flask application needs a mongodb database to store data. 

In your mongodb instance, run:

```mongosh
> use TestDB
TestDB> use t_TestDB
t_TestDB> use admin
admin> db.createUser({'user': 'TestUser', 'pwd': 'test', 'roles': [{'role': 'readWrite', 'db': 'TestDB'}, {'role': 'readWrite'}}, 'db': 't_TestDB'}]})
```

This creates two database `TestDB` and `t_TestDB` (for the tests) and a user `TestUser` with read/write privileges on both

Then, a mongodb URI should be defined for this to work with proper authentication, etc.

To be used with the Flask application example here, tTis can be achieved in 2 ways. First approach consists in defining ENV variables, MONGO_DEV_URI to run the app and MONGO_TEST_URI to test the app through pytest. Second approach consists in declaring these ENV variables in the shell on your system before running the application and/or the tests.

Typical MONGO URI looks like: ```mongodb://<username>:<pwd>/127.0.0.1:27017/<db>?authSource=admin```. The last part (authSource) is needed as the users have been created in the `admin` part of the mongo instance.

### Running the Flask Application

Run development server to serve the Flask application:

```sh
(.venv) $ flask --app app --debug run
```

Navigate to 'http://127.0.0.1:5000' in your favorite web browser to view the website!

## Key Python Modules Used

* **Flask**: micro-framework for web application development which includes the following dependencies:
  * click: package for creating command-line interfaces (CLI)
  * itsdangerous: cryptographically sign data 
  * Jinja2: templating engine
  * MarkupSafe: escapes characters so text is safe to use in HTML and XML
  * Werkzeug: set of utilities for creating a Python application that can talk to a WSGI server
  * ShortUUID: to generate unique IDs
* **pytest**: framework for testing Python projects
* **Flask-PyMongo** - mongodb for Flask
* **Flask-Login** - support for user management (login/logout) in Flask
* **Flask-WTF** - simplifies forms in Flask
* **flake8** - static analysis tool

This application is written using Python 3.10.

## Testing

To run all the tests:

```sh
(.venv) $ python -m pytest -v
```

To check the code coverage of the tests:

```sh
(.venv) $ python -m pytest --cov-report term-missing --cov=project
```
